#! /usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Project "purgeFiles"
    Script for deleting old and obsolete files.
    see readme.md for more details
    Version 2.0.2, 19/juin/23
"""

import argparse
import os
import glob
import platform
import datetime
import xml.etree.ElementTree as ET
import shutil
from pathlib import Path
import pczMiscLib as myLib
import pczLogger
import pczMailer

SCRIPTNAME = "purgeFiles"
DEFAULT_CONFIG_PATH = f"./{SCRIPTNAME}.config.xml"
TS_FORMAT = "%d-%m-%Y %H:%M:%S"

g_logger = None


def main():
    global g_logger
    try:
        rc = 9
        bMailer = False
        bLog = False
        bEmailOnlyIfError = False

        ts = datetime.datetime.now()
        sStartingDateTime = ts.strftime(TS_FORMAT)
        sHostName = platform.node()

        argParser = argparse.ArgumentParser(description="purgeFiles - delete old files")
        argParser.add_argument(
            "configPath",
            nargs="?",
            default=DEFAULT_CONFIG_PATH,
            help="Path for the configuration file",
        )
        args = argParser.parse_args()
        if args.configPath is not None:
            if not os.path.isfile(args.configPath):
                raise myLib.PczException(
                    "argument #1 (Configuration file) is an invalide path"
                )

        xDocConfig = ET.parse(args.configPath)
        xRoot = xDocConfig.getroot()

        sLogLevel = (
            xRoot.find("logging").get("level")
            if xRoot.find("logging").get("level") is not None
            else "info"
        )
        iLogMaxBytes = (
            int(xRoot.find("logging").get("maxbytes"))
            if xRoot.find("logging").get("maxbytes") is not None
            else 20480
        )
        iLogBackupCount = (
            int(xRoot.find("logging").get("backupcount"))
            if xRoot.find("logging").get("backupcount") is not None
            else 10
        )
        sLogFolder = (
            xRoot.find("logging").get("folder")
            if xRoot.find("logging").get("folder") is not None
            else "./"
        )
        sLogName = (
            xRoot.find("logging").get("name")
            if xRoot.find("logging").get("name") is not None
            else "purgefiles"
        )

        g_logger = pczLogger.PczLogger(
            logLevel=sLogLevel,
            logFolder=sLogFolder,
            logName=sLogName,
            logFileName=f"{SCRIPTNAME}.log",
            backupCount=iLogBackupCount,
            logMaxBytes=iLogMaxBytes,
        )
        bLog = True

        g_logger.info("Starting script at {0}".format(sStartingDateTime))
        g_logger.debug("Configuration file: " + args.configPath)

        # Mailer
        if xRoot.find("./mailer").get("configPath") is not None:
            sMailerConfigPath = xRoot.find("./mailer").get("configPath")
            myMailer = pczMailer.PczMailer()
            (bResult, sError) = myMailer.initFromXml(sMailerConfigPath)
            if not bResult:
                raise myLib.PczException(f"'{sError}' occured in Mailer.initFromXml()")
            else:
                bMailer = True
        else:
            g_logger.error(
                f"Attribut 'mailer.configPath' is not defined in configuration file"
            )
        if bMailer:
            if xRoot.find("./mailer").get("from") is not None:
                sFrom = xRoot.find("./mailer").get("from")
            else:
                g_logger.error(f"Attribut 'from' is not defined in .config.xml file")
                bMailer = False
            if xRoot.find("./mailer").get("to") is not None:
                sTo = xRoot.find("./mailer").get("to")
            else:
                g_logger.error(f"Attribut 'to' is not defined in .config.xml file")
                bMailer = False

            if xRoot.find("./mailer").get("onlyIfError") is not None:
                bEmailOnlyIfError = myLib.convertString2Bool(
                xRoot.find("./mailer").get("onlyIfError")
            )
            else:
                bEmailOnlyIfError = False

        ## Boucle pour l'acquisition des differents traitements de purge (<purge>)
        for xPurge in xRoot.findall("./purges/purge"):
            # cas balises facultatives
            if xPurge.find("./targetDirectory") is not None:
                sTargetDirectory = xPurge.find("./targetDirectory").text
            else:
                sTargetDirectory = ""
            if xPurge.find("./numberToKeep") is not None:
                iNumberToKeep = int(xPurge.find("./numberToKeep").text)
            else:
                iNumberToKeep = 0
            sFolder = xPurge.find("./rootDirectory").text
            sExtension = xPurge.find("./extension").text
            sDelayType = xPurge.find("./delay").get("type")
            iDelayValue = int(xPurge.find("./delay").text)
            sAction = xPurge.get("action")
            bDeleteEmptyFolder = myLib.convertString2Bool(
                xPurge.get("deleteEmptyFolder")
            )
            g_logger.info(
                f"Purge folder '{sFolder}' for Action={sAction}, with filters as : extension= {sExtension}, delay= {iDelayValue} {sDelayType}, NumberToKeep= {iNumberToKeep}"
            )
            purgeDirectory(
                sFolder,
                sExtension,
                sDelayType,
                iDelayValue,
                sAction,
                bDeleteEmptyFolder,
                sTargetDirectory,
                iNumberToKeep,
            )

        ## Fin du script

        rc = 0
    except myLib.PczException as e:
        g_logger.error("MyException: " + e.message)
        rc = 1
    except Exception as e:
        g_logger.exception("EXCEPTION, Fatale error : " + e.message)
        rc = -1
    finally:
        if bLog:
            g_logger.info("Ending script with ReturnCode= {0}\n".format(rc))
        else:
            print("Ending script with ReturnCode= {0}\n".format(rc))

        if bMailer:
            if rc == 0:
                if not bEmailOnlyIfError:
                    sMessage = "purgeFiles is done on {1} with NO error - TimeStamp= {0} \n See .log file in attach for details.\n".format(ts, sHostName)
                else:
                    sMessage = ""
            else:
                sMessage = "purgeFiles is done on {1} WITH error(s) - TimeStamp= {0} \n See .log file in attach for details.\n".format(ts, sHostName)
            if len(sMessage) >0:
                # add all log files in sLogFolder
                sAttachs = ""
                for p in scanDirRecurse(sLogFolder):
                    if os.path.isfile(p) == False:
                        continue
                    sAttachs = p if sAttachs == "" else sAttachs + "," + p

                # Create email
                (bAdd, sError) = myMailer.addMail(
                    sender=sFrom,
                    to=sTo,
                    subject="purgeFiles report - {0}".format(ts),
                    message=sMessage,
                    format="text",
                    attachments=sAttachs,
                )
                if not bAdd:
                    print(f"'{sError}' occured in addMail()")

        return rc
    # EndOf main()


def purgeDirectory(
    sRootFolder,
    sExtension,
    sDelayType,
    iDelayValue,
    sAction,
    bDeleteEmptyFolder,
    sTargetDirectory,
    iNumberToKeep,
):
    for p in scanDirRecurse(sRootFolder):
        if os.path.isfile(p) == False:
            continue
        fileDT = datetime.datetime.fromtimestamp(os.path.getmtime(p))
        g_logger.debug(
            f"Analysis of the file '{p}' ({fileDT.strftime(TS_FORMAT)} / delay={calcDelayFromToday(sDelayType, p)} {sDelayType})."
        )
        if filterExtension(sExtension, Path(p).name) == False:
            g_logger.debug(f"Extension not match for '{p}'")
            continue
        if filterDelay(sDelayType, iDelayValue, p) == False:
            g_logger.debug(f"Delay not match for '{p}'")
            continue
        if (
            iNumberToKeep > 0
            and getFilesNumberInFolder(os.path.dirname(p)) <= iNumberToKeep
        ):
            # g_logger.info( f" Fichier '{p}' conservé car il reste {getFilesNumberInFolder(os.path.dirname(p))} fichiers sur (limite={a_numberToKeep})" )
            g_logger.info(
                f" File '{p}' kept because there are {getFilesNumberInFolder(os.path.dirname(p))} file(s) left in the folder (limite={iNumberToKeep})"
            )
            continue

        # On réalise l'action prévue
        # g_logger.debug( f"File '{p}' selected for action:'{sAction.upper()}'.")
        if sAction.lower() == "none" or sAction.lower() == "print":
            # Nothing to do except PRINTING
            g_logger.info(
                f"PRINT '{p}' ({fileDT.strftime(TS_FORMAT)} / delay={calcDelayFromToday(sDelayType, p)} {sDelayType}) "
            )
        elif sAction.lower() == "delete":
            os.remove(p)
            g_logger.info(" DELETE '{0}' ".format(p))
        elif sAction.lower() == "move":
            # Calcul du sous-répertoire cible
            sRelativeSubFolders = getSubFolders(sRootFolder, os.path.dirname(p))
            sNewFolder = os.path.join(sTargetDirectory, sRelativeSubFolders)
            g_logger.debug(
                f"Create folder '{sNewFolder}' - root='{sRootFolder}', relative='{sRelativeSubFolders}' "
            )
            os.makedirs(sNewFolder, exist_ok=True)
            # move = copy + delete
            shutil.copy2(p, sNewFolder)
            if sAction.lower() == "move":
                os.remove(p)
                g_logger.info("   MOVE '{0}' TO '{1}'".format(p, sNewFolder))
            else:
                g_logger.info("   COPY '{0}' TO '{1}'".format(p, sNewFolder))
    if bDeleteEmptyFolder:
        # suppression des répertoires vides dans l'arborescence Source
        bPrintTitle = False
        for root, dirs, files in os.walk(sRootFolder, topdown=False):
            for name in dirs:
                if isEmptyFolder(os.path.join(root, name)):
                    if not bPrintTitle:
                        g_logger.info("Try to remove empty folder(s) :")
                        bPrintTitle = True
                    g_logger.info(
                        "  Folder '{0}' deleted".format(os.path.join(root, name))
                    )
                    os.rmdir(os.path.join(root, name))
    # endOf purgeDirectory()


def sorter(p):
    ts = Path(p).stat().st_mtime
    dt = datetime.datetime.fromtimestamp(ts)
    sDt = dt.strftime("%Y%m%d.%H%M%S")
    str = "{0}|{1}".format(Path(p).parents[0], sDt)
    # g_logger.debug("sorted() '{0}' pour Path({1}).mtime={2}".format(str,p, dt) )
    return str


def scanDirRecurse(a_rootFolder):
    """refactoring for <numberToKeep> : sorted by date-time in eatch folders"""
    if not os.path.isdir(a_rootFolder):
        raise myLib.PczException(f"'{a_rootFolder}' not exist !")
    arFiles = glob.glob(a_rootFolder + "/**/*", recursive=True)
    arFiles = sorted(arFiles, key=sorter, reverse=False)
    # map(Path, arFiles) # convert string path to Path class
    return arFiles
    # EndOf scanDirRecurse()


def isEmptyFolder(a_folderPath):
    iNbrEntries = 0
    for root, dirs, files in os.walk(a_folderPath, topdown=True):
        for name in files:
            # sPath = os.path.join(root, name) #AVIRER
            iNbrEntries += 1
        for name in dirs:
            # sPath = os.path.join(root, name) #AVIRER
            iNbrEntries += 1
    if iNbrEntries > 0:
        return False
    else:
        return True


def getSubFolders(a_rootFolder, a_currentFolder):
    if not a_currentFolder.startswith(a_rootFolder):
        raise myLib.PczException(
            f"Error getSubFolders() - '{a_rootFolder}' is not included in '{a_currentFolder}'"
        )
    iLgSubFolders = len(a_currentFolder) - len(a_rootFolder)
    if iLgSubFolders == 0:
        sRelativeSubFolder = ""
    else:
        sRelativeSubFolder = a_currentFolder[-iLgSubFolders:]
        if sRelativeSubFolder.startswith("\\") or sRelativeSubFolder.startswith("/"):
            sRelativeSubFolder = sRelativeSubFolder[1:]
    return sRelativeSubFolder


def filterExtension(a_extentions, a_fileName):
    for ext in a_extentions.split(","):
        if ext == "*":
            return True
        if a_fileName.lower().endswith("." + ext.lower()):
            return True
    return False


def calcDelayFromToday(a_delayType, a_path):
    fileDT = datetime.datetime.fromtimestamp(os.path.getmtime(a_path))
    todayDT = datetime.datetime.today()
    deltaDT = todayDT - fileDT
    # g_logger.debug("DELTA DT = {}".format(deltaDT.days) )
    if a_delayType.lower() == "day":
        return deltaDT.days
    elif a_delayType.lower() == "week":
        return round((deltaDT.days / 7), 0)
    elif a_delayType.lower() == "month":
        return round((deltaDT.days / 30), 0)
    else:
        raise ValueError(
            f"a_delayType not supported. Current value: {a_delayType}. Supported values: days, week"
        )


def filterDelay(a_delayType, a_delayValue, a_path):
    if calcDelayFromToday(a_delayType, a_path) >= a_delayValue:
        return True
    return False


def getFilesNumberInFolder(a_folderPath):
    """How many files in the folder (not recursive)"""
    iNbrEntries = 0
    arDirs = os.listdir(a_folderPath)
    for f in arDirs:
        if os.path.isfile(os.path.join(a_folderPath, f)):
            iNbrEntries += 1
    return iNbrEntries


if __name__ == "__main__":
    rc = main()
    exit(rc)
