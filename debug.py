#! /usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Script for debugging Functions of purgeFiles.py
"""

import argparse
import os
import glob
import sys
import time
import datetime
import xml.etree.ElementTree as ET
import shutil
import logging
import logging.handlers
from pathlib import Path

TS_FORMAT = "%d-%m-%Y %H:%M:%S"



def scanDirRecurse( a_rootFolder) :
    """ refactoring for <numberToKeep> : sorted by date-time in eatch folders """
    arFiles = glob.glob(a_rootFolder + '/**/*', recursive=True)
    arFiles = sorted( arFiles, key = sorter, reverse=False )
    #map(Path, arFiles) # convert string path to Path class
    return arFiles
    #EndOf scanDirRecurse()


def sorter( p ) :
    ts = Path(p).stat().st_mtime
    dt = datetime.datetime.fromtimestamp(ts)
    sDt = dt.strftime("%Y%m%d.%H%M%S")
    str = "{0}-{1}".format(Path(p).parents[0], sDt)
    print("{0} - {1}".format(str, p))
    return str


def getFilesNumberInFolder( a_folderPath ):
    """ How many files in the folder (not recursive) """
    iNbrEntries = 0
    arDirs = os.listdir(a_folderPath)
    for f in arDirs:
        if os.path.isfile(os.path.join(a_folderPath,f)): 
            iNbrEntries += 1
    print( "getFilesNumberInFolder({0}) = {1}.".format( a_folderPath, iNbrEntries) )
    return iNbrEntries

def calcDelayFromToday(  a_delayType, a_path ):
    fileDT = datetime.datetime.fromtimestamp(os.path.getmtime(a_path))
    todayDT = datetime.datetime.today()
    deltaDT = todayDT - fileDT
    #logger.debug("DELTA DT = {}".format(deltaDT.days) )
    if a_delayType.lower() == "day":
        return deltaDT.days
    if a_delayType.lower() == "week":
        return round((deltaDT.days / 7),0)
    #TODO: si MONTH
    #if a_delayType.lower() == "month":
    #    if deltaDT.months >= a_delayValue :
    #TODO: from dateutil.relativedelta import relativedelta / https://stackoverflow.com/questions/546321/how-do-i-calculate-the-date-six-months-from-the-current-date-using-the-datetime
    #https://pythonhosted.org/MonthDelta/

def filterDelay( a_delayType, a_delayValue, a_path ):
    if calcDelayFromToday( a_delayType, a_path ) >= a_delayValue :
        return True
    return False


# main()
a_numberToKeep = 2
a_rootFolder = 'C:\\TESTS\\purgeFiles\\TESTS\\2 MOVE\\'
a_delayType = "week"

for p in scanDirRecurse( a_rootFolder ):
    #if de.is_file() == False :
    #    continue
    fileDT = datetime.datetime.fromtimestamp(os.path.getmtime(p))
    print( "Analyse fichier '{0}' ({1} / delay={2} {3}).".format( p, fileDT.strftime(TS_FORMAT), calcDelayFromToday(a_delayType, p), a_delayType) )
    if a_numberToKeep >0 and getFilesNumberInFolder(os.path.dirname(p)) <= a_numberToKeep :
        continue
