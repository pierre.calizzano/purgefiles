# README of purgeFiles
Deleting obsolete files. (*Python version of an old .vbs windows script.*)

### INSTALLATION
No installation required  : just run the script `purgeFiles.py`. 
But need python files of *pczlib* in the same directory.

`python3 ./purgeFiles.py ./my_purgefiles.config.xml`

**Configuration file** is an **optional** parameter. `purgefiles.config.xml` is the default configuration file.

It is recommended to run this script from a Windows scheduled task or a linux crontab.
In this case, it is necessary to specify the full paths to files invoked in the command line.
The account launching the script must have the necessary rights to delete or move files.


### CONFIGURATION
The configuration is performed by the XML file, specified as a startup argument.

The XML file format is as follows:
```
<root>
    <logging level="info" folder="/home/you/mnb/log/" name="purgeFiles" />
    <mailer configPath="/home/you/develop/pczlib/pczMailer.config.xml" from="purgefiles@you.me" to="test@you.me" />
    <purges>
        <purge action="delete|move|print"  deleteEmptyFolder="true">
            <rootDirectory purgeSubDir="true">/home/you/myFolderForTests</rootDirectory>
            <targetDirectory>/home/you/archives</rootDirectory>
            <delay type="Day|Week|Month">9</delay>						
            <extension>tif,jpeg,tmp</extension> <!-- * for any extension-->
            <numberToKeep>1</numberToKeep>
        </purge>	
    </purges>
</root>
```

Description of the parameters.

- `purges.*` : List of purge profiles. Each profile has its own parameters (directory, extension, retention time ...)
    - `purge`
        - `action` : 
            - delete : each selected file is physically removed from the.
            - move : each selected file is moved into the tree structure defined by the <targetDirectory> tag
            - none|print : no action on the file. Useful to have logs of selected files.
        - `deleteEmptyFolder` : If "true", deletion of subdirectories that have become empty due to file deletion.
        - `purges.purge.rootDirectory` : Root of the directory tree to be analyzed.
        - `purgeSubDir` : determines whether to scan the subdirectories or not.
        - `purges.purge.targetDirectory` : Root of the target tree for 'move' action.  Note: the subtree is preserved
        - `delay` : Retention period of the files. Value specified in day (type="Day") or week (type="Week") or month (type="Month")  Note: Is't not realy 'month', it is "1 month"="30 days".
        - `extension` : List of file extensions to select. The extensions are separated by a comma. The character '*' causes the selection of all files.
        - `numberToKeep` : Minimum number of files to keep (all extensions included).
- `logging` : LOG configuration
    - `level` : 
        - debug : debug + info + error
        - info  : error + information log
        - error : Only error log
- `mailer` : Send log by email with script pczmailer.py (planned in version 1.2)
    - `configPath`: path to config file
    - `from`: email from
    - `to`: email to

